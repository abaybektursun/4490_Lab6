-- YOU WILL NEED APPROPRIATE ENVIRONEMNT FOR RESULTS TO BE SEPARATED INTO SEPARATE TABS

drop table has_contacts;
drop table sends_messages;
drop table users;

create table users(
    username varchar(30) not null,
    password VARBINARY(128) not null
);

create table sends_messages(
    from_usr varchar(30) not null,
    to_usr varchar(30) not null,
    message varchar(900),
    date_sent datetime not null DEFAULT NOW()
);

create table has_contacts ( 
    username varchar(30),
    contact varchar(30)
);

alter table users
    add constraint users_uname_pk primary key(username);

alter table sends_messages
    add constraint sendsms_from_fk foreign key(from_usr)
    references users(username);
alter table sends_messages
    add constraint sendsms_to_fk foreign key(to_usr)
    references users(username);

    
alter table has_contacts
    add constraint hasc_user_fk foreign key(username)
    references users(username);
alter table has_contacts
    add constraint hasc_contact_fk foreign key(contact)
    references users(username);
    
insert into users values("jsmith@uca.edu",   aes_encrypt("hello123" , "jsmith@uca.edu"  ));
insert into users values("msmith@uca.edu",   aes_encrypt("pass123"  , "msmith@uca.edu"  ));
insert into users values("tjones@yahoo.com", aes_encrypt("123456"   , "tjones@yahoo.com"));
insert into users values("jjones@yahoo.com", aes_encrypt("hello1234", "jjones@yahoo.com"));

insert into has_contacts values("jsmith@uca.edu","msmith@uca.edu")  ;
insert into has_contacts values("jsmith@uca.edu","tjones@yahoo.com");
insert into has_contacts values("msmith@uca.edu","tjones@yahoo.com");

insert into sends_messages values("jsmith@uca.edu",   "msmith@uca.edu",	"Hello",     now());
insert into sends_messages values("tjones@yahoo.com",	"msmith@uca.edu",	"How r u?",  now());
insert into sends_messages values("tjones@yahoo.com",	"jsmith@uca.edu",	"Hello Joe", now());

commit;

select username, aes_decrypt(password,username)
from users;  

select * from has_contacts;
select * from sends_messages;

